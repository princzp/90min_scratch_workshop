Teach the very basics of programming to teenagers.
(Age 12-14, and the content is geared towards girls, specifically, but should work for boys as well.)

This is a 90 minutes Scratch intro workshop for kids aged 12-14.
The individual projects are interim savepoints with approx. 10 minutes increments.

The projects are uploaded here as well:
https://scratch.mit.edu/studios/3457182/

